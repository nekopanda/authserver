# Authorization Server #

* User 1 

    * username : `user001`
    * password : `teststaff`

* User 2

    * username : `user002`
    * password : `testmanager`

* Client 1

    * client_id : `client001`
    * client_secret : `abcd`
    
## Flow Authorization Code ##

1. Akses Authorization URL : [http://localhost:8080/oauth/authorize?client_id=client001&grant_type=authorization_code&response_type=code](http://localhost:8080/oauth/authorize?client_id=client001&grant_type=authorization_code&response_type=code)

2. Bila belum login, maka login dulu

3. Setelah login, akan ditampilkan consent page. Approve scope yang diinginkan

    [![Consent Page](docs/01-consent-page.png)](docs/01-consent-page.png)

4. Berikutnya, browser akan diredirect. Ambil authorization code yang ada di address bar

    [![Auth Code](docs/02-authcode.png)](docs/02-authcode.png)

5. Tukarkan authorization code menjadi token

    [![Penukaran AuthCode](docs/03-authcode-to-access-token.png)](docs/03-authcode-to-access-token.png)

6. Cek isi token 

    [![Isi Token](docs/04-token-info.png)](docs/04-token-info.png)