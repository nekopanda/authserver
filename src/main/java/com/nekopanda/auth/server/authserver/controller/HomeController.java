package com.nekopanda.auth.server.authserver.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class HomeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @PreAuthorize("hasAnyAuthority('VIEW_TRANSAKSI', 'EDIT_TRANSAKSI')")
    @GetMapping("/home")
    public ModelMap home(Authentication currentUser) {
        LOGGER.info("Current user : "+currentUser);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:ss");
        return new ModelMap().addAttribute("today", sdf.format(new Date()));
    }
}
